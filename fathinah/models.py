from django.db import models


# hanya menyimpan 24 elemen terakhir
def keep_last_24(MyModel):
    most_recents = MyModel.objects.all()
    if (len(most_recents) > 24):
        last24 = MyModel.objects.all()[len(most_recents) - 24:]
        MyModel.objects.exclude(id__in=last24).delete()


class Map(models.Model):
    map = models.TextField(max_length=20, default="")
    city = models.TextField(max_length=20, default="")

    mapPict = models.ImageField(upload_to="store_img/map_data/map", height_field=None, width_field=None,
                                max_length=None, null=True)
    cityPict = models.ImageField(upload_to="store_img/map_data/city", height_field=None, width_field=None,
                                 max_length=None, null=True)


class Data(models.Model):
    ObjectPhoto = models.ImageField(upload_to="store_img/map_data/uploads", height_field=None, width_field=None,
                                    max_length=None, null=True)
    map = models.ForeignKey(Map, on_delete=models.CASCADE)

    class meta:
        ordering = ['id']

    def save(self, *args, **kwargs):
        super(Data, self).save(*args, **kwargs)

        keep_last_24(Data)
