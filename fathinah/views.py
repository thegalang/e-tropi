from django.shortcuts import render, redirect
from .forms import DataForm
from .models import Data, Map
import random, os

history_link = 'hist_cek_peta'

# fungsi yang merender object ke-id
def display_object(request, obj_id):
    data = Data.objects.get(id=obj_id)
    context = {}
    context['img_data'] = data
    context['form'] = DataForm()
    context['history'] = Data.objects.all()
    context['history_link'] = history_link
    return render(request, "peta.html", context)


def selectRandomMap(data):

    selected = Map.objects.order_by('?')[0]
    data.map = selected

def index(request):
    context = {}
    if request.method == "POST":

        form = DataForm(request.POST, request.FILES)
        if (form.is_valid()):

            data = Data(ObjectPhoto=request.FILES['data'])

            selectRandomMap(data)

            data.save()

            return redirect(display_object, data.id)

    form = DataForm()
    context['form'] = form
    context['history'] = Data.objects.all()
    context['history_link'] = history_link
    return render(request, 'peta.html', context)
