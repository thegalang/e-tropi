from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import Map, Data
from PIL import Image
from io import BytesIO
from django.core.files.base import File
from django.http import HttpRequest


# membuat foto untuk test
def random_image(name='test.png', ext='png', size=(20, 20), color=(256, 0, 0)):
    file_obj = BytesIO()
    image = Image.new("RGBA", size=size, color=color)
    image.save(file_obj, ext)
    file_obj.seek(0)
    return File(file_obj, name=name)


# membuat textfile
def random_textfile(name="test.txt", ext='txt'):
    file_obj = BytesIO()
    file_obj.write(b"this is just a test textfile!")

    with open(name, "wb") as outfile:
        outfile.write(file_obj.getbuffer())

    file_obj.seek(0)
    return File(file_obj, name=name)


class CekItemUnitTest(TestCase):

    def test_cek_peta_exist_cek_peta(self):
        response = Client().get('/cek_peta/')
        self.assertEqual(response.status_code, 200)

    def test_using_index(self):
        found = resolve('/cek_peta/')
        self.assertEqual(found.func, views.index)

    def test_storepage_contains_general(self):
        request = HttpRequest()
        response = views.index(request)
        self.assertContains(response, "Upload item here",
                            count=1, html=True)

    # mengecek apakah model dapat save data
    def test_data_saving_works(self):

        test_map = Map(cityPict=random_image(), mapPict=random_image(),
                       city="Depok", map="Indonesia", id=1)

        test_map.save()

        test_data = Data(id=1, ObjectPhoto=random_image(), map = test_map)
        test_data.save()

        self.assertIsInstance(test_data, Data)
        self.assertEqual(Data.objects.count(), 1)

    # test saat buat post request apakah nambah
    def test_post_request_valid(self):

        test_image = random_image()

        test_map = Map(cityPict=random_image(), mapPict=random_image(),
                       city="Depok", map="Indonesia", id=1)
        test_map.save()

        response = Client().post('/cek_peta/', {'data': test_image}, follow=True)

        self.assertEqual(Data.objects.count(), 1)

        response = Client().get('/cek_peta/1')
        self.assertEqual(response.status_code, 200)

    # test jika masukkin data invalid
    def test_post_request_invalid(self):
        test_textfile = random_textfile()

        response = Client().post('/cek_peta/', {'data': test_textfile}, follow=True)

        self.assertEqual(Data.objects.count(), 0)

    # ngecek apakah bener ukuran maksimum dari databasenya 24
    def test_max_size_of_db(self):

        test_map = Map(cityPict=random_image(), mapPict=random_image(),
                       city="Depok", map="Indonesia", id=1)

        test_map.save()
        # mengisi data database dengan 30 elemen
        for id_obj in range(1, 31):
            test_data = Data(id=id_obj, ObjectPhoto=random_image(), map = test_map)
            test_data.save()

        # karena simpan 24 data terakhir, maka yang berada di database dari 7 - 30
        self.assertEquals(Data.objects.filter(id=30).count(), 1)
        self.assertEquals(Data.objects.filter(id=7).count(), 1)
        self.assertEquals(Data.objects.filter(id=6).count(), 0)
        self.assertEquals(Data.objects.all().count(), 24)

    def test_peta(self):
        test_map = Map(cityPict=random_image(), mapPict=random_image(),
                       city="Depok", map="Indonesia", id=1)

        test_map.save()

        response = Client().post('/cek_peta/', {'data': random_image()}, follow=True)

        self.assertContains(response, "The item is originated from")
        self.assertContains(response, "Depok, Indonesia",
                            count=1, html=True)

