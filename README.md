[![pipeline status](https://gitlab.com/thegalang/e-tropi/badges/master/pipeline.svg)](https://gitlab.com/thegalang/e-tropi/commits/master)

# e-tropi
Kelompok: KB03

Nama-nama anggota kelompok:
1. Fathinah Asma Izzati
2. Galangkangin Gotera
3. Muhammad Zuhdi Zamrud
4. Salsabila Adnan

## Deskripsi:
Membuat website sebagai pusat informasi untuk barang-barang asli dan KW. Pada website kami, kita bisa mengetahui berbagai jenis informasi mengenai barang-barang KW. Seperti negara asalnya, indikator-indikator KWnya, dan perbandingannya dengan barang asli.

## Manfaat:
Kami berharap dengan ide dari website ini, industri konsumen dapat menjadi lebih positif. Produsen barang-barang akan lebih cermat dalam membuat barang-barangnya sehingga tidak cacat dan diklasifikasikan KW, dan juga penipuan yang disebabkan oleh barang-barang KW dapat dikurangi.

## Daftar fitur yang akan diimplementasikan:

### 1. Mengecek keaslian barang.

Pada laman ini, user dapat mengupload foto. Lalu, akan ditampilkan apakah barang tersebut asli atau kw. Jika barang tersebut asli, laman ini juga akan menampilkan artis-artis yang juga memakai barang tersebut. Diharapkan ini meningkatkan motivasi pemakai dalam memakai barangnya.

### 2. Mengecek negara asal barang tersebut

Pada laman ini, user dapat mengupload foto. Lalu, baik asli maupun palsu, akan ditampilkan daerah asal dari barang tersebut. Daerah ini berbentuk peta yang dimana provinsinya akan ditandai lalu diberikan foto satelit dari daerah produksinya. 

Misal, apabila sebuah barang KW berasal dari Beijing, China, Akan ada dua buah gambar peta. Satu gambar menandai provinsi beijing pada peta. Satu gambar lagi memberikan foto satelit dari gambar tersebut tentang letak daerah tersebut.

### 3 Mengecek indikator-indikator KW barang tersebut

Pada fitur ini, user dapat mengupload foto. Lalu, apabila barang tersebut palsu, akan ditampilkan apa saja indikator-indikator yang membuat barang tersebut palsu. Misalnya, barang dapat dikatakan palsu apabila tidak memiliki logo mereknya, sisi lebih melengkung dari produk asli, ataupun teksturnya tidak sesuai dengan aslinya.

### 4. Membandingkan barang tersebut dengan barang aslinya

Pada fitur ini, user dapat mengupload foto. Jika barang tersebut palsu, akan ditampilkan barang aslinya sebagai cara untuk membandingkan dua barang tersebut. Kami tujukan fitur ini untuk para kurator muda yang ingin belajar caranya membedakan barang asli dan palsu.

Kami juga menyediakan fitur “history” untuk setiap laman. Pada setiap laman, user dapat melihat foto-foto apa saja yang sudah pernah di kirim oleh user-user lain. User dapat mengklik foto tersebut dan akan menampilkan informasi yang sesuai untuk foto tersebut.


