from django.db import models

# Create your models here.
def save_last_24(AnyModel):
    recents = AnyModel.objects.all()
    if(len(recents)>24):
        last_24 = AnyModel.objects.all()[len(recents) - 24:]
        AnyModel.objects.exclude(id__in=last_24).delete()

class Data(models.Model):
    ObjectPhoto = models.ImageField(upload_to="store_img/compare_item_data/uploaded", height_field=None, width_field=None, max_length=None, null=True)
    Genuine = models.CharField(max_length= 10)
    Brand = models.CharField(max_length=15)
    Price = models.PositiveIntegerField()
    Release_date = models.PositiveIntegerField()
    Location = models.CharField(max_length=20)

    class meta:
        ordering = ['id']

    def save(self, *args, **kwargs):
        super(Data, self).save(*args, **kwargs)
        save_last_24(Data)