from django.shortcuts import render, reverse, redirect
from .forms import DataForm
from .models import Data
import random
# Create your views here.

def display_item(request, item_id):
    data_item = Data.objects.get(id=item_id)
    context = {}
    context['img_data'] = data_item
    context['form'] = DataForm()
    context['history'] = Data.objects.all()
    return render(request, "compare_item/index.html", context)

def compare_item(request):
    context = {}
    if(request.method == "POST"):
        form = DataForm(request.POST, request.FILES)
        if(form.is_valid()):

            genuine = "Genuine"
            list_brand = ["Gucci", "H&M", "Hermes", "Uniqlo", "Zara", "Adidas", "Chanel", "Burberry", "Prada", "Levi's"]
            list_location = ["Singapore", "America", "Canada", "Germany"]
            release_date = random.randrange(1999,2019)
            price = random.randrange(150,2000)
            brand = list_brand[random.randrange(9)]
            location = list_location[random.randrange(4)]
            num = random.random()
            if (num>0.7):
                genuine = "Counterfeit"

            data_item = Data(Genuine=genuine, ObjectPhoto=request.FILES['ObjectPhoto'], Brand=brand,Release_date=release_date,Price=price,Location=location)
            data_item.save()
            
            return redirect('compare:display_item',data_item.id)
        else:
            return redirect('compare:compare_item')
    elif(request.method == "GET"):
        form = DataForm()
        context['form'] = form
        context['history'] = Data.objects.all()
        
        return render(request, 'compare_item/index.html', context)
