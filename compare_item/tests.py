from django.test import TestCase
from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import Data
from PIL import Image
from io import BytesIO
from django.core.files.base import File
from django.http import HttpRequest

# Create your tests here.

def random_image(name = 'test.png', ext='png', size=(20, 20), color=(256,0,0)):
	file_obj = BytesIO()
	image = Image.new("RGBA", size=size, color=color)
	image.save(file_obj, ext)
	file_obj.seek(0)
	return File(file_obj, name=name)

# membuat textfile
def random_textfile(name = "test.txt", ext='txt'):

	file_obj = BytesIO()
	file_obj.write(b"this is just a test textfile!")

	with open(name, "wb") as outfile:
		outfile.write(file_obj.getbuffer())

	file_obj.seek(0)
	return File(file_obj, name=name)

class CompareItemUnitTest(TestCase):

    def test_compare_item_exist(self):
        response = Client().get('/compare_item/')
        self.assertEqual(response.status_code, 200)
        
    def test_using_compare_item(self):
        found = resolve('/compare_item/')
        self.assertEqual(found.func, views.compare_item)
    
    def test_storepage_contains_general(self):
        response = Client().get("/compare_item/")
        self.assertContains(response, "Upload the item you want to compare:",count = 1, html=True)
    
    def test_data_saving_works(self):
        test_data = Data(id= 1,Genuine="Genuine", ObjectPhoto=random_image(), Brand="Gucci",Release_date=2001,Price=280,Location="Singapore")
        test_data.save()
        self.assertIsInstance(test_data, Data)
        self.assertEqual(Data.objects.count(),1)
    
    def test_post_request_valid(self):
        test_image = random_image()
        response  = Client().post('/compare_item/',{'ObjectPhoto':test_image}, follow=True)
        self.assertEqual(Data.objects.count(),1)
        
        response = Client().get('/compare_item/1')
        
        self.assertEqual(response.status_code,200)
        
    def test_post_request_invalid(self):
        test_textfile = random_textfile()
        response = Client().post('/compare_item/', {'ObjectPhoto' : test_textfile}, follow=True)
        self.assertEqual(Data.objects.count(), 0)
    
    def test_random_genuine_working(self):

		# coba post 24 barang random dan cek apakah ada genuine dan counterfeit
        for i in range(24):
            test_image = random_image()
            response = Client().post('/compare_item/', {'ObjectPhoto' : test_image}, follow = True)
            

        # karena data banyak, maka pasti ada setidaknya satu barang genuine dan counterfeit
        
        self.assertEqual(Data.objects.count(),24)
        self.assertTrue(Data.objects.filter(Genuine = "Genuine").count() > 0)
        self.assertTrue(Data.objects.filter(Genuine = "Counterfeit").count() > 0)

    def test_fake_item_page(self):

        test_data = Data(id= 1,Genuine="Counterfeit", ObjectPhoto=random_image(), Brand="Gucci",Release_date=2001,Price=280,Location="Singapore")
        test_data.save()

        self.assertEqual(test_data.Genuine, "Counterfeit")

        request = HttpRequest()
        response = views.display_item(request, 1)

        self.assertEquals(response.status_code, 200)
        self.assertNotContains(response, "Brand", html=True)

    def test_genuine_item_page(self):

        test_data = Data(id= 1,Genuine="Genuine", ObjectPhoto=random_image(), Brand="Gucci",Release_date=2001,Price=280,Location="Singapore")
        test_data.save()

        self.assertEqual(test_data.Genuine, "Genuine")

        request = HttpRequest()
        response = views.display_item(request, 1)

        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "Brand", count=1,html=True)

    def test_max_size_of_db(self):

        # mengisi data database dengan 30 elemen
        for id_obj in range(1, 31):
        	test_data = Data(id=id_obj,Genuine="Genuine", ObjectPhoto=random_image(), Brand="Gucci",Release_date=2001,Price=280,Location="Singapore")
        	test_data.save()

          # karena simpan 24 data terakhir, maka yang berada di database dari 7 - 30
        
        self.assertEquals(Data.objects.filter(id=30).count(), 1)
        self.assertEquals(Data.objects.filter(id=7).count(), 1)
        self.assertEquals(Data.objects.filter(id=6).count(), 0)
        self.assertEquals(Data.objects.all().count(), 24)
