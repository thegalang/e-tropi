from django.urls import path, include
from compare_item import views

urlpatterns = [
    path('', views.compare_item, name="compare_item"),
	path("<int:item_id>", views.display_item, name="display_item")
]