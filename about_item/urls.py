from django.contrib import admin 
from django.urls import include, path 
from django.conf import settings 
from django.conf.urls.static import static
from about_item import views

urlpatterns = [
	path('', views.index, name='home'),
	path("<int:obj_id>", views.display_object, name='history_about_item')
] 
