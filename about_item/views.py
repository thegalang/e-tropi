from django.shortcuts import render, redirect
from .forms import UploadForm
from .models import UploadModel
import random

history_link = 'history_about_item'

material_selection = ["Leather", "Cotton", "Nylon", "Canvas", "Straw", "Denim", "Neoprene", "Mesh"]

year_selection = [str(x) for x in range(2010, 2020)]

# fungsi yang merender object ke-id
def display_object(request, obj_id):

	data = UploadModel.objects.get(id = obj_id)
	context = {}
	context['img_data'] = data
	context['form'] = UploadForm()
	context['history'] = UploadModel.objects.all()
	context['history_link'] = history_link
	return render(request, "about_item/index.html", context)

def index(request):

	context = {}
	if request.method == "POST":

		form = UploadForm(request.POST, request.FILES)
		if(form.is_valid()):

			data = UploadModel(ObjectPhoto = request.FILES['data'],
							   material = random.sample(material_selection, 1)[0],
							   price = random.randint(100000, 10000000),
							   year = random.sample(year_selection, 1)[0])

			data.save()

			return redirect(display_object, data.id)			

	form  = UploadForm()
	context['form'] = form
	context['history'] = UploadModel.objects.all()
	context['history_link'] = history_link
	return render(request, 'about_item/index.html', context)