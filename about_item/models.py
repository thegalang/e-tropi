from django.db import models

# Create your models here.
def keep_last_24(MyModel):
	most_recents = MyModel.objects.all()
	if(len(most_recents)>24):
		last24 = MyModel.objects.all()[len(most_recents) - 24:]
		MyModel.objects.exclude(id__in=last24).delete()

class UploadModel(models.Model):
	
	ObjectPhoto = models.ImageField(upload_to="store_img/about_item_data/uploaded", height_field=None, width_field=None, max_length = None, null = True )
	material = models.CharField(max_length = 15)
	price = models.PositiveIntegerField()
	year = models.CharField(max_length = 6)

	class meta:
		ordering = ['id']

	def save(self, *args, **kwargs):

		super(UploadModel, self).save(*args, **kwargs)

		keep_last_24(UploadModel)