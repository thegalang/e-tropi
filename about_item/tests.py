from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import UploadModel
from PIL import Image
from io import BytesIO
from django.core.files.base import File
from django.http import HttpRequest

# membuat foto untuk test
def random_image(name = 'test.png', ext='png', size=(20, 20), color=(256,0,0)):
	file_obj = BytesIO()
	image = Image.new("RGBA", size=size, color=color)
	image.save(file_obj, ext)
	file_obj.seek(0)
	return File(file_obj, name=name)

# membuat textfile
def random_textfile(name = "test.txt", ext='txt'):

	file_obj = BytesIO()
	file_obj.write(b"this is just a test textfile!")

	with open(name, "wb") as outfile:
		outfile.write(file_obj.getbuffer())

	file_obj.seek(0)
	return File(file_obj, name=name)


class AboutItemUnitTest(TestCase):

	def test_about_item_exist_about_item(self):
		response = Client().get('/about_item/')
		self.assertEqual(response.status_code, 200)

	def test_using_index(self):
		found = resolve('/about_item/')
		self.assertEqual(found.func, views.index)

	def test_storepage_contains_general(self):
		request = HttpRequest()
		response = views.index(request)
		self.assertContains(response, "UPLOAD ITEM FOR DETAILS",
							count = 1, html=True)

	# mengecek apakah model dapat save data
	def test_data_saving_works(self):
		test_data = UploadModel(id=1, ObjectPhoto = random_image(), material = "cotton", price="1000", year = 2015)
		test_data.save()

		self.assertIsInstance(test_data, UploadModel)
		self.assertEqual(UploadModel.objects.count(),1)	

	# test saat buat post request apakah nambah
	def test_post_request_valid(self):
		
		test_image = random_image()

		response = Client().post('/about_item/', {'data': test_image}, follow=True)

		self.assertEqual(UploadModel.objects.count(), 1)
		
		response = Client().get('/about_item/1')
		self.assertEqual(response.status_code, 200)

	# test jika masukkin data invalid
	def test_post_request_invalid(self):

		test_textfile = random_textfile()

		response = Client().post('/about_item/', {'data' : test_textfile}, follow=True)

		self.assertEqual(UploadModel.objects.count(), 0)

	# ngecek apakah bener ukuran maksimum dari databasenya 24
	def test_max_size_of_db(self):

		# mengisi data database dengan 30 elemen
		for id_obj in range(1, 31):
			test_data = UploadModel(id=id_obj, ObjectPhoto = random_image(),  material = "cotton", price="1000", year = 2015)
			test_data.save()

		# karena simpan 24 data terakhir, maka yang berada di database dari 7 - 30
		self.assertEquals(UploadModel.objects.filter(id=30).count(), 1)
		self.assertEquals(UploadModel.objects.filter(id=7).count(), 1)
		self.assertEquals(UploadModel.objects.filter(id=6).count(), 0)
		self.assertEquals(UploadModel.objects.all().count(), 24)
