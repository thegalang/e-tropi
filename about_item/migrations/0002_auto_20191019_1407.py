# Generated by Django 2.2.5 on 2019-10-19 07:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('about_item', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='uploadmodel',
            name='material',
            field=models.CharField(default='cotton', max_length=15),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='uploadmodel',
            name='price',
            field=models.PositiveIntegerField(default=1000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='uploadmodel',
            name='year',
            field=models.CharField(default=2012, max_length=6),
            preserve_default=False,
        ),
    ]
