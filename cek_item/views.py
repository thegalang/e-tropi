from django.shortcuts import render, redirect
from .forms import DataForm
from .models import Data, CelebrityPhoto
import random, os

history_link = 'history_cek_item'

def landing_page(request):
	return redirect(index)

# fungsi yang merender object ke-id
def display_object(request, obj_id):

	data = Data.objects.get(id = obj_id)
	context = {}
	context['img_data'] = data
	context['form'] = DataForm()
	context['history'] = Data.objects.all()
	context['history_link'] = history_link
	return render(request, "cek_item/index.html", context)

# pilih 8 foto random untuk setiap objek
def populate_celebs(data):

	selected = CelebrityPhoto.objects.order_by('?')[:8]
	for celeb in selected:
		data.Celebrities.add(celeb)

def index(request):

	context = {}
	if request.method == "POST":

		form = DataForm(request.POST, request.FILES)
		if(form.is_valid()):

			# menentukan apakah barang asli atau palsu secara random
			# peluang 30% barang palsu
			genuine = "Genuine"
			num = random.random()
			if(num>0.7):
				genuine = "Counterfeit"

			data = Data(Genuine = genuine, 
						ObjectPhoto = request.FILES['data'])

			data.save()

			populate_celebs(data)
			data.save()

			return redirect(display_object, data.id)			

	form  = DataForm()
	context['form'] = form
	context['history'] = Data.objects.all()
	context['history_link'] = history_link
	return render(request, 'cek_item/index.html', context)