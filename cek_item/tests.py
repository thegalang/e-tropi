from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import CelebrityPhoto, Data
from PIL import Image
from io import BytesIO
from django.core.files.base import File
from faker import Faker
from django.http import HttpRequest

# membuat foto untuk test
def random_image(name = 'test.png', ext='png', size=(20, 20), color=(256,0,0)):
	file_obj = BytesIO()
	image = Image.new("RGBA", size=size, color=color)
	image.save(file_obj, ext)
	file_obj.seek(0)
	return File(file_obj, name=name)

# membuat textfile
def random_textfile(name = "test.txt", ext='txt'):

	file_obj = BytesIO()
	file_obj.write(b"this is just a test textfile!")

	with open(name, "wb") as outfile:
		outfile.write(file_obj.getbuffer())

	file_obj.seek(0)
	return File(file_obj, name=name)


# mengisi data celeb dengan data random
def populate_celeb_db(CelebrityPhoto, fake_data_num):
	fake = Faker()
	for i in range(fake_data_num):
		celeb_data = CelebrityPhoto(Name = fake.name(), Photo = random_image())
		celeb_data.save()

class CekItemUnitTest(TestCase):

	# cek apakah ini berfungsi sebagai landing_page
	def test_cek_item_exist_landing(self):
		response = Client().get('')
		self.assertRedirects(response, '/cek_item/')

	def test_cek_item_exist_cek_item(self):
		response = Client().get('/cek_item/')
		self.assertEqual(response.status_code, 200)

	def test_using_index(self):
		found = resolve('/cek_item/')
		self.assertEqual(found.func, views.index)

	def test_storepage_contains_general(self):
		request = HttpRequest()
		response = views.index(request)
		self.assertContains(response, "Upload the item you want to check:",
							count = 1, html=True)

	# mengecek apakah model dapat save data
	def test_data_saving_works(self):
		test_data = Data(id=1, Genuine="Genuine", ObjectPhoto = random_image())
		test_data.save()
		views.populate_celebs(test_data)
		test_data.save()

		self.assertIsInstance(test_data, Data)
		self.assertEqual(Data.objects.count(),1)

		celeb_data = CelebrityPhoto(Name="John Doe", Photo = random_image())
		celeb_data.save()

		self.assertIsInstance(celeb_data, CelebrityPhoto)
		self.assertEqual(CelebrityPhoto.objects.count(), 1)

	# test saat buat post request apakah nambah
	def test_post_request_valid(self):
		
		test_image = random_image()

		response = Client().post('/cek_item/', {'data': test_image}, follow=True)

		self.assertEqual(Data.objects.count(), 1)
		
		response = Client().get('/cek_item/1')
		self.assertEqual(response.status_code, 200)

	# test jika masukkin data invalid
	def test_post_request_invalid(self):

		test_textfile = random_textfile()

		response = Client().post('/cek_item/', {'data' : test_textfile}, follow=True)

		self.assertEqual(Data.objects.count(), 0)

	# test fungsi random apakah mungkin memberikan item Genuine dan Counterfeit
	# random saya memberikan peluang 0.7 untuk sebuah item sebagai Genuine dan 0.3 sebagai counterfeit
	def test_random_genuine_working(self):

		# coba post 24 barang random dan cek apakah ada genuine dan counterfeit
		for i in range(24):
			test_image = random_image()
			response = Client().post('/cek_item/', {'data' : test_image}, follow = True)

		self.assertEquals(Data.objects.count(), 24)

		# karena data banyak, maka pasti ada setidaknya satu barang genuine dan counterfeit
		self.assertTrue(Data.objects.filter(Genuine = "Genuine").count() > 0)
		self.assertTrue(Data.objects.filter(Genuine = "Counterfeit").count() > 0)


	# mengecek apakah item palsu tidak mencetak artis
	def test_fake_item_page(self):

		test_data = Data(id=1, Genuine="Counterfeit", ObjectPhoto = random_image())
		test_data.save()

		populate_celeb_db(CelebrityPhoto, 10)

		views.populate_celebs(test_data)

		self.assertEqual(test_data.Genuine, "Counterfeit")

		request = HttpRequest()
		response = views.display_object(request, 1)

		self.assertEquals(response.status_code, 200)
		self.assertNotContains(response, "The item is also used by",
							   html=True)

	# mengecek apakah item asli mencetak artis
	def test_genuine_item_page(self):

		test_data = Data(id = 1, Genuine="Genuine", ObjectPhoto = random_image())
		test_data.save()

		populate_celeb_db(CelebrityPhoto, 10)

		views.populate_celebs(test_data)

		self.assertEqual(test_data.Genuine, "Genuine")
		request = HttpRequest()
		response = views.display_object(request, 1)

		self.assertEquals(response.status_code, 200)
		self.assertContains(response, "The item is also used by",
							count=1, html = True)

	def test_data_giving_8_celebs(self):

		test_data = Data(id=1, Genuine = "Genuine", ObjectPhoto = random_image())
		test_data.save()

		populate_celeb_db(CelebrityPhoto, 20)

		views.populate_celebs(test_data)
		test_data.save()

		self.assertEqual(test_data.Celebrities.count(), 8)

	# ngecek jika jumlah celeb < 8
	def test_data_giving_n_celebs(self):

		test_data = Data(id=1, Genuine="Genuine", ObjectPhoto = random_image())
		test_data.save()
		
		populate_celeb_db(CelebrityPhoto, 3)

		views.populate_celebs(test_data)
		test_data.save()

		self.assertEqual(test_data.Celebrities.count(), 3)

	# ngecek apakah bener ukuran maksimum dari databasenya 24
	def test_max_size_of_db(self):

		# mengisi data database dengan 30 elemen
		for id_obj in range(1, 31):
			test_data = Data(id=id_obj, Genuine="Genuine", ObjectPhoto = random_image())
			test_data.save()

		# karena simpan 24 data terakhir, maka yang berada di database dari 7 - 30
		self.assertEquals(Data.objects.filter(id=30).count(), 1)
		self.assertEquals(Data.objects.filter(id=7).count(), 1)
		self.assertEquals(Data.objects.filter(id=6).count(), 0)
		self.assertEquals(Data.objects.all().count(), 24)