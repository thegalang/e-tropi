from django.db import models

# hanya menyimpan 24 elemen terakhir
def keep_last_24(MyModel):
	most_recents = MyModel.objects.all()
	if(len(most_recents)>24):
		last24 = MyModel.objects.all()[len(most_recents) - 24:]
		MyModel.objects.exclude(id__in=last24).delete()

class CelebrityPhoto(models.Model):
	
	Name = models.TextField(max_length = 20, default = "", primary_key = True)
	Photo = models.ImageField(upload_to="store_img/cek_item_data/celebrity",height_field=None, width_field=None, max_length = None, null = True )

	class meta:
		verbose_name_plural = "celebrity photos"
		
class Data(models.Model):

	Celebrities = models.ManyToManyField(CelebrityPhoto)
	ObjectPhoto = models.ImageField(upload_to="store_img/cek_item_data/uploaded", height_field=None, width_field=None, max_length = None, null = True )
	Genuine = models.CharField(max_length = 10)

	class meta:
		ordering = ['id']

	def save(self, *args, **kwargs):

		super(Data, self).save(*args, **kwargs)

		keep_last_24(Data)

		