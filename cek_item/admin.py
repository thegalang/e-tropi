from django.contrib import admin
from . import models

admin.site.register(models.CelebrityPhoto)
admin.site.register(models.Data)
